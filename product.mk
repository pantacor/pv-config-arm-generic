TARGET_OS := linux
TARGET_OS_FLAVOUR := generic
TARGET_LIBC := eglibc

TARGET_ARCH := arm

TARGET_CROSS := $(ALCHEMY_WORKSPACE_DIR)/prebuilt/arm-linux-musleabihf/bin/arm-linux-musleabihf-

TARGET_GLOBAL_CFLAGS += -march=armv6 -mfloat-abi=hard -mfpu=vfp
TARGET_DEFAULT_ARM_MODE = arm

HOST_CPP = cc -E

TARGET_IMAGE_FORMAT := cpio
TARGET_FINAL_MODE := firmware

TARGET_SKEL_DIRS := $(TOP_DIR)/trail/arm-generic/config/
